
from ROOT import TGeoManager, TGeoIterator, gGeoManager
from collections import defaultdict
import json

TGeoManager.Import("LHCb.gdml")
nextnode = TGeoIterator(gGeoManager.GetMasterVolume())

allnodes=[]
nodesperdet=defaultdict(list)

# Iterating through the nodes to dump the names in a text file
# Also attempting to count the number of nodes per subdetector
# as the paths are of the kind:
# _dd_Geometry_DownstreamRegion_Ecal_Modules_InnCellPb_InnerPbFiber_70xdf9dbf0
# we split accordingly

n = nextnode()
while n:
    # Updating the full list
    allnodes.append(n.GetName())

    # trying to determine the subdetector
    namesplit = n.GetName().split("_")
    det = "none"
    if len(namesplit)>=5:
        det=namesplit[4]
    nodesperdet[det].append(n.GetName())

    # and moving on...
    n = nextnode()
    


for k in nodesperdet:
    print("%s -> %s" % (k, len(nodesperdet[k])))

with open("allnodes.txt", "wt") as f:
    for n in allnodes:
        f.write(n + "\n")

with open("nodesperdet.json", "wt") as f:
    json.dump(nodesperdet, f)



