#!/bin/sh

gaudirun.py export.py \$GAUSSOPTS/Gauss-Upgrade.py \$DECFILESROOT/options/10000000.py \$LBPYTHIA8ROOT/options/Pythia8.py \$GAUSSROOT/tests/options/testGauss-config-defaults.py
