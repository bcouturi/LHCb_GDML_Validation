LHCb Upgrade Geometry export test
==================================

This directory containes the script needed to export the LHCb upgrade geometry to GDML.

The options were run using the lhcb-gauss-dev build (as the MR https://gitlab.cern.ch/lhcb/Gauss/merge_requests/510 allowing the export is not merged at this stage).
The file LHCbDump.gdml was produced and some errors were found when linting against the schema for GDML, and loading in Geant4. The outputs are:

  - xmllint.log: the output of "xmllint --schema GDMLSchema/gdml.xsd LHCbDump.gdml --noout"
  - load_gdml.log: the out of loading with the Geant4 load_gdml example (from LCG 96b)
  
