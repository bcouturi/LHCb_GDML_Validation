from __future__ import print_function
from collections import namedtuple


import xml.etree.ElementTree as ET
tree = ET.parse('LHCb.gdml')
root = tree.getroot()


def process_node(node, tagstofind, current_path = ""):
    new_path = "/".join([current_path, node.tag]) 
    if node.tag in tagstofind:
        print(new_path)
    for c in node:
         process_node(c, tagstofind, new_path)

unknown_tags = [ "bordersurface", "matrix", "opticalsurface", "physvolref", "property" ]

process_node(root, unknown_tags)
