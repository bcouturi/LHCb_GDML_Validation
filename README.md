LHCb GDML export validation 
==================================

Initial Status
==================================

Producing the GDML
------------------

It was exported from Gauss using the following command:
```
lb-run --platform=x86_64-centos7-gcc62-opt Gauss/v52r2 gaudirun.py GDMLWriter.py \$GAUSSOPTS/Gauss-Upgrade.py \$DECFILESROOT/options/10000000.py \$LBPYTHIA8ROOT/options/Pythia8.py \$GAUSSROOT/tests/options/testGauss-config-defaults.py
```

GDMLWriter.py being modified to be:
```
from Gaudi.Configuration import *

def addGDML():
    from Configurables import GiGa, GiGaRunActionSequence
    giga = GiGa()
    giga.addTool( GiGaRunActionSequence("RunSeq") , name="RunSeq" )
    giga.RunSeq.Members.append( "GDMLRunAction" )


appendPostConfigAction(addGDML)
```

Loading in ROOT
---------------

Loading with the trivial ROOT macro:

```
void geom_lhcb()
{  
  gSystem->Load("libGeom");
  TGeoManager::Import("LHCb.gdml");
  gGeoManager->SetVisLevel(8);
  gGeoManager->GetMasterVolume()->Draw("ogl");
}
```

Produces a display, but also the following log:

```
Processing geom_lhcb.C...
Info in <TGeoManager::Import>: Reading geometry from file: LHCb.gdml
Info in <TGeoManager::TGeoManager>: Geometry GDMLImport, Geometry imported from GDML created
Error: Unsupported GDML Tag Used :matrix. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :matrix. Please Check Geometry/Schema.
[...]
Error: Unsupported GDML Tag Used :matrix. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :matrix. Please Check Geometry/Schema.
Error in <TGeoSphere::SetDimensions>: invalid parameters theta1/theta2
Error in <TGeoSphere::SetDimensions>: invalid parameters theta1/theta2
Error: Unsupported GDML Tag Used :opticalsurface. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :property. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :property. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :opticalsurface. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :property. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :property. Please Check Geometry/Schema.
[...]
Error: Unsupported GDML Tag Used :opticalsurface. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :property. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :property. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :bordersurface. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :physvolref. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :physvolref. Please Check Geometry/Schema.
[...]
Error: Unsupported GDML Tag Used :bordersurface. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :physvolref. Please Check Geometry/Schema.
Error: Unsupported GDML Tag Used :physvolref. Please Check Geometry/Schema.
Info in <TGeoManager::SetTopVolume>: Top volume is World. Master volume is World
Info in <TGeoNavigator::BuildCache>: --- Maximum geometry depth set to 100
Info in <TGeoManager::CheckGeometry>: Fixing runtime shapes...
Info in <TGeoManager::CheckGeometry>: ...Nothing to fix
Info in <TGeoManager::CloseGeometry>: Counting nodes...
Info in <TGeoManager::Voxelize>: Voxelizing...
Info in <TGeoManager::CloseGeometry>: Building cache...
Info in <TGeoManager::CountLevels>: max level = 9, max placements = 12969
Info in <TGeoManager::CloseGeometry>: 18542546 nodes/ 1922 volume UID's in Geometry imported from GDML
Info in <TGeoManager::CloseGeometry>: ----------------modeler ready----------------
Info in <TGeoManager::SetVisLevel>: Automatic visible depth disabled
Info in <TCanvas::MakeDefCanvas>:  created default TCanvas with name c1
```

Checking the missing tags they are:  "bordersurface", "matrix", "opticalsurface", "physvolref", "property" 


Now looking where they are used in LHCb GDML, with their path and count:

```
     84 /gdml/define/matrix
     44 /gdml/materials/material/property
     20 /gdml/solids/opticalsurface
     40 /gdml/solids/opticalsurface/property
     20 /gdml/structure/bordersurface
     40 /gdml/structure/bordersurface/physvolref
```


I couldn't find bordersurface in the Geant4 doc, but the XSD decribes it as the "Surface between two physical volumes", hence the two physvolref.
Do we need those bordersurfaces defined at all ?

opticalsurface is the "Optical surface used by Geant4 optical processes". We probably do not need this either.

We however have an issue with the definition of materials, as show in the exerpt from LHCb.gdml:

```
<define>
    <matrix coldim="2" name="RINDEX" values="1.5e-06 1.0013 1.52e-06 [...]
    <matrix coldim="2" name="GROUPVEL" values="1.5e-06 299.398 1.51e-06 [...]
    <matrix coldim="2" name="ABSLENGTH" values="1.5e-06 1e+15 2e-06 1e+[...]
    <matrix coldim="2" name="CKVRNDX" values="1.75e-06 1.0013 1.76944e-[...]
    <matrix coldim="2" name="RINDEX" values="9e-07 1.46 1e-06 1.46 1.5[...]
    <matrix coldim="2" name="GROUPVEL" values="9e-07 205.337 9.5e-07 [...]
    <matrix coldim="2" name="ABSLENGTH" values="9e-07 1e+18 1e-06 1e[...]
    <matrix coldim="2" name="CKVRNDX" values="1.75e-06 1.46 2e-06 1[...]
    <matrix coldim="2" name="REFLECTIVITY" values="1e-06 0 2e-06 0[...]
[...]
```

Those matrices are afterwards used in materials.
Maybe this is not an issue either, as we can migrate the materials once and for all to GDML that can be read by ROOT.


Loading into Geant4 
-------------------

G01 GDML examples produces a lot of errors when loading LHCb.gdml (c.f. [g01_load_validation.log](g01_load_validation.log) )

Dima mentioned they had this problem when the GeantV team tried the LHCb Geometry, and they found a workaround.
You need to recompile Sim/LbGDML to set  *storeReferences* to true in G4GDMLParser.write.

This produces the updated [LHCb.gdml](LHCb.gdml).

This procedure removes part of the problems, when loading in Geant4 G01 example (c.f. [g01_load_validation_take2.log] (g01_load_validation_take2.log)).
However, we still get many errors like:
```
G4GDML: VALIDATION ERROR! value '/dd/Materials/Pipe/PipeBeTV560x110867b0' is invalid NCName at line: 252
```

That need to be investigated.


Validation with xmllint
-----------------------

I also tried checking LHCb.gdml aginst the GDML XSD from geant4.10.04.p02, running

```
xmllint --schema gdml_schema/gdml.xsd LHCb.gdml --noout
```

You can see the output in xmllint.log


Fixing the export (2018-10-01)
==============================

Gauss fixes
-----------

The merge request:

https://gitlab.cern.ch/lhcb/Gauss/merge_requests/346

contains trivial fixes to:
  * export unique names for the elements in GDML (appending the pointer value), this is an option in the GDML export
  * Fix the GDMLWriter to move the algorithm configuration in a post config action 

A patched version of Geant4, found in the Geant4-srcs GIT repo, called fix_gdml_export

https://gitlab.cern.ch/lhcb/Geant4-srcs/commits/fix_gdml_export

has to be used. 

This can be done by building the LHCb Geant4 project with the variable *GEANT4_TAG* set to *fix_gdml_export*

When this is done, the exported GDML seems valid, here is the log of the load_gdml command:


```
$ ./vanilla_g4/install/bin/load_gdml LHCb.gdml

Usage: load_gdml <intput_gdml_file:mandatory> <output_gdml_file:optional>

G4GDML: Reading 'LHCb.gdml'...
G4GDML: Reading definitions...
G4GDML: Reading materials...
G4GDML: Reading solids...
G4GDML: Reading structure...
G4GDML: Reading setup...
G4GDML: Reading 'LHCb.gdml' done!
Stripping off GDML names of materials, solids and volumes ...

**************************************************************
 Geant4 version Name: geant4-10-04-patch-02    (25-May-2018)
                       Copyright : Geant4 Collaboration
                      References : NIM A 506 (2003), 250-303
                                 : IEEE-TNS 53 (2006), 270-278
                                 : NIM A 835 (2016), 186-225
                             WWW : http://geant4.org/
**************************************************************

<<< Geant4 Physics List simulation engine: FTFP_BERT 2.0


 FTFP_BERT : new threshold between BERT and FTFP is over the interval 
 for pions :   3 to 12 GeV
 for kaons :   3 to 12 GeV
 for proton :  3 to 12 GeV
 for neutron : 3 to 12 GeV

### Adding tracking cuts for neutron  TimeCut(ns)= 10000  KinEnergyCut(MeV)= 0
Visualization Manager instantiating with verbosity "warnings (3)"...
Visualization Manager initialising...
Registering graphics systems...

You have successfully registered the following graphics systems.
Current available graphics systems are:
ASCIITree (ATree)
DAWNFILE (DAWNFILE)
G4HepRep (HepRepXML)
G4HepRepFile (HepRepFile)
RayTracer (RayTracer)
VRML1FILE (VRML1FILE)
VRML2FILE (VRML2FILE)
gMocrenFile (gMocrenFile)

Registering model factories...

You have successfully registered the following model factories.
Registered model factories:
  generic
  drawByAttribute
  drawByCharge
  drawByOriginVolume
  drawByParticleID
  drawByEncounteredVolume

Registered filter factories:
  attributeFilter
  chargeFilter
  originVolumeFilter
  particleFilter
  encounteredVolumeFilter

You have successfully registered the following user vis actions.
Run Duration User Vis Actions: none
End of Event User Vis Actions: none
End of Run User Vis Actions: none

Some /vis commands (optionally) take a string to specify colour.
"/vis/list" to see available colours.


Global auxiliary info:


Available UI session types: [ GAG, tcsh, csh ]
ERROR: Can not open a macro file <vis.mac>. Set macro path with "/control/macroPath" if needed.
Idle> 
```

Fixing the Sphere theta1/theta2 error (2018-10-05)
==================================================

During the ROOT import, we faced the following error:

```
Error in <TGeoSphere::SetDimensions>: invalid parameters theta1/theta2
Error in <TGeoSphere::SetDimensions>: invalid parameters theta1/theta2
```

iMarkus found out this was due to he following sphere definition:

```
<sphere aunit="deg" deltaphi="360" deltatheta="24.2497130131093" lunit="mm" name="VacTankDSExitWindow0xda59a90" rmax="980" rmin="978" startphi="0" starttheta="155.750286986891"/>
```

where deltatheta + startheta = 180.0000000000003 > 180

One solution is to sanitize the GDML writer in G4. ROOT should also be protected at import.


Test with upgraded ROOT (2019-04-03)
====================================

Fixing the theta1/theta2 error, and using a recent version of ROOT, we now get:
(commit 34d11e63a19cdef49a9a36e2852b27179545ef73 from march 19th)


```

$ root geom_lhcb.C 
   ------------------------------------------------------------
  | Welcome to ROOT 6.17/01                  https://root.cern |
  |                               (c) 1995-2019, The ROOT Team |
  | Built for linuxx8664gcc on Mar 20 2019, 14:55:00           |
  | From heads/master@v6-16-00-rc1-1344-g34d11e63a1            |
  | Try '.help', '.demo', '.license', '.credits', '.quit'/'.q' |
   ------------------------------------------------------------

root [0] 
Processing geom_lhcb.C...
Info in <TGeoManager::Import>: Reading geometry from file: LHCb.gdml
Info in <TGeoManager::TGeoManager>: Geometry GDMLImport, Geometry imported from GDML created
Info in <TGeoManager::SetTopVolume>: Top volume is World. Master volume is World
Info in <TGeoNavigator::BuildCache>: --- Maximum geometry depth set to 100
Info in <TGeoManager::CheckGeometry>: Fixing runtime shapes...
Info in <TGeoManager::CheckGeometry>: ...Nothing to fix
Info in <TGeoManager::CloseGeometry>: Counting nodes...
Info in <TGeoManager::Voxelize>: Voxelizing...
Info in <TGeoManager::CloseGeometry>: Building cache...
Info in <TGeoManager::CountLevels>: max level = 9, max placements = 12969
Info in <TGeoManager::CloseGeometry>: 18542546 nodes/ 1922 volume UID's in Geometry imported from GDML
Info in <TGeoManager::CloseGeometry>: ----------------modeler ready----------------
Info in <TGeoManager::SetVisLevel>: Automatic visible depth disabled
Info in <TCanvas::MakeDefCanvas>:  created default TCanvas with name c1


```

It seems that load errors have gone.


